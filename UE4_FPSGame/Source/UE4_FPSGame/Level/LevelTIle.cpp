// Copyrigth Dominik Pavlíček.

#include "LevelTIle.h"
#include "Engine/World.h"

// Sets default values
ALevelTIle::ALevelTIle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ALevelTIle::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void ALevelTIle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ALevelTIle::PlaceActors(TSubclassOf<AActor>ToSpawn, int MinSpawnRate, int MaxSpawnRate)
{
	if (MinSpawnRate == 0 || MinSpawnRate == NULL)
	{
		MinSpawnRate = IntMin;
	}

	if (MaxSpawnRate == 0 || MaxSpawnRate == NULL)
	{
		MaxSpawnRate = IntMax;
	}

	if (MinSpawnRate > MaxSpawnRate)
	{
		MaxSpawnRate = MinSpawnRate + 1;
	}
	
	FVector MinPoint(0, -2000, 0);
	FVector MaxPoint(4000, 2000, 0);
	int32 RandomInt = FMath::RandRange(MinSpawnRate, MaxSpawnRate);
	
	FBox BoxBounds(MinPoint, MaxPoint);

	for (int i = 0; i < RandomInt; i++)
	{
		float RandomFloat = FMath::RandRange(0, 360);
		FRotator RandomRotation(0.f, RandomFloat, 0.f);
		
		FVector RandomLocation = FMath::RandPointInBox(BoxBounds);
		AActor* SpawnedActor = GetWorld()->SpawnActor<AActor>(ToSpawn);
		SpawnedActor->SetActorLocation(RandomLocation);
		SpawnedActor->SetActorRotation(RandomRotation);
		SpawnedActor->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
	}
}


// Copyrigth Dominik Pavlíček.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LevelTIle.generated.h"

UCLASS()
class UE4_FPSGAME_API ALevelTIle : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALevelTIle();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/*
	This function will randomly spawn selected Actor.
	This Actor will be spawned x times (amount is randomly selected from Min and Max Range).
	If the Min or Max range doesn't contain any data or is equal 0, the functions uses default values which are:
	- min: 8
	- max: 16 
	If the Min value is > than Max value, the Max value is automatically set to Min value + 1
	*/
	UFUNCTION(BlueprintCallable, Category = SpawnActors)
		void PlaceActors(TSubclassOf<AActor>ToSpawn, int MinSpawnRate, int MaxSpawnRate);
	
	// Defines minimum number of Assets to be spawned in the Tile
	UPROPERTY(EditDefaultsOnly, Category = SpawnActors)
		int32 IntMin = 8;

	// Defines maximum number of Assets to be spawned in the Tile. Please, keep in mind that spawning too much Assets might be very performance heavy!
	UPROPERTY(EditDefaultsOnly, Category = SpawnActors)
		int32 IntMax = 16;

};
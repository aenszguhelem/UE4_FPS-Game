// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE4_FPSGameGameMode.generated.h"

UCLASS(minimalapi)
class AUE4_FPSGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUE4_FPSGameGameMode();
};




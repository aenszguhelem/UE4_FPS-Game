// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "UE4_FPSGameGameMode.h"
#include "UE4_FPSGameHUD.h"
#include "UE4_FPSGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUE4_FPSGameGameMode::AUE4_FPSGameGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AUE4_FPSGameHUD::StaticClass();
}

// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "UE4_FPSGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UE4_FPSGame, "UE4_FPSGame" );
 
// Copyrigth Dominik Pavlíček.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AComponent_PatrolRoute.generated.h"

/*
Component that holds Patrol Points, used for Patrol Route definition.
AITask_Patrol Class is using this AComponent_PatrolRoute Class for setting up the Patrol Points.
*/

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UE4_FPSGAME_API UAComponent_PatrolRoute : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAComponent_PatrolRoute();
	TArray<AActor*>GetPatrolPoints() const;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	UPROPERTY(EditInstanceOnly, Category = "PATROL POINTS")
		TArray<AActor*> PatrolPoints;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};

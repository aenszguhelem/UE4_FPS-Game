// Copyrigth Dominik Pavlíček.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GunActor.generated.h"

class UE4_FPSGameHUD;

UCLASS()
class UE4_FPSGAME_API AGunActor : public AActor
{
	GENERATED_BODY()

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USkeletalMeshComponent* FP_Gun;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USceneComponent* FP_MuzzleLocation;
			
public:	
	// Sets default values for this actor's properties
	AGunActor();

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		TSubclassOf<class AUE4_FPSGameProjectile> ProjectileClass;
	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class USoundBase* FireSound;

	/** AnimMontage of FPP */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class UAnimMontage* FPP_FireAnimation;

	/** AnimMontage of TPP */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class UAnimMontage* TPP_FireAnimation;

	/** AnimInstance of FPP */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Gameplay)
		class UAnimInstance* FPP_AnimInstance;

	/** AnimInstance of TPP */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Gameplay)
		class UAnimInstance* TPP_AnimInstance;

	/** Fires a projectile. */
	UFUNCTION(BlueprintCallable, Category = Weapon)
		void OnFire();

	UFUNCTION(BlueprintCallable, Category = Weapon)
		USceneComponent* GetMuzzleLocation() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;	
};
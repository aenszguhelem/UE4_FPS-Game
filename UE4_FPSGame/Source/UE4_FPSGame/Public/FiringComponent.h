// Copyrigth Dominik Pavlíček.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "FiringComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UE4_FPSGAME_API UFiringComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFiringComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION(BlueprintCallable, Category = "Projectile")
		void SpawnProjectile(UClass* Projectile, FRotator SpawnRotation, FVector SpawnLocation);
	
};

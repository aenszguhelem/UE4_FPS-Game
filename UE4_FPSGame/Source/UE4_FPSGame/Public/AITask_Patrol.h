// Copyrigth Dominik Pavlíček.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "AITask_Patrol.generated.h"

class UBlackboardComponent;

/*
 AITask_Patrol function is supposted to keep track on WayPoints for AI.
 This function sets NextWayPoint each time AI Player arrives to the current one.
 */
UCLASS()
class UE4_FPSGAME_API UAITask_Patrol : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	UPROPERTY(EditAnywhere, Category = "Blackboard")
		struct FBlackboardKeySelector Index;
	UPROPERTY(EditAnywhere, Category = "Blackboard")
		struct FBlackboardKeySelector WayPointKey;
private:
	APawn* ControlledPawn = nullptr;
	UBlackboardComponent* BlackboardComponent = nullptr;
};

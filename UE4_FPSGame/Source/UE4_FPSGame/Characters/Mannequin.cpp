// Copyrigth Dominik Pavlíček.

#include "Mannequin.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"


// Sets default values
AMannequin::AMannequin()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a CameraComponent	
	FPPCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FPPCamera"));
	FPPCamera->SetupAttachment(GetCapsuleComponent());
	FPPCamera->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FPPCamera->bUsePawnControlRotation = true;

	FPPMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("BasicFPPBodyMesh"));
	FPPMesh->SetOnlyOwnerSee(true);
	FPPMesh->SetupAttachment(FPPCamera);
	FPPMesh->bCastDynamicShadow = false; // default value false
	FPPMesh->CastShadow = false; // default value false
	FPPMesh->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	FPPMesh->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);
}

// Called when the game starts or when spawned
void AMannequin::BeginPlay()
{
	Super::BeginPlay();
	if (GunBlueprint == NULL)
	{
		UE_LOG(LogTemp, Warning, TEXT("Mannequin || BeginPlay() || NO AGunActor FOR FPS CHARACTER"))
			return;
	}

	Gun = GetWorld()->SpawnActor<AGunActor>(GunBlueprint);
	if (IsPlayerControlled())
	{
		Gun->AttachToComponent(FPPMesh, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint")); // attaches the Gun to the Grip Point
		Gun->FPP_AnimInstance = FPPMesh->GetAnimInstance();
	}
	else
	{
		Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
		Gun->TPP_AnimInstance = GetMesh()->GetAnimInstance();
	}
	
	if (InputComponent != NULL)
	{
		InputComponent->BindAction("Fire", IE_Pressed, this, &AMannequin::PullTrigger);
	}
}

void AMannequin::OnUnPossess()
{
	if (Gun)
	{
		Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
	}
}
	
// Called every frame
void AMannequin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AMannequin::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AMannequin::PullTrigger()
{
	Gun->OnFire();
}

// Copyrigth Dominik Pavlíček.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "public/GunActor.h"
#include "Mannequin.generated.h"

UCLASS()
class UE4_FPSGAME_API AMannequin : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USkeletalMeshComponent* FPPMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FPPCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TPPCamera;

public:
	// Sets default values for this character's properties
	AMannequin();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool EnableTouchscreenMovement(UInputComponent* InputComponent);

	// Function that reattach the Gun from FPP to TPP
	UFUNCTION(BlueprintCallable, Category = Weapon)
		void OnUnPossess();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditDefaultsOnly, Category = Setup)
		TSubclassOf<class AGunActor> GunBlueprint;
	
	// Function Triggers a Firing from Gun
	UFUNCTION(BlueprintCallable, Category = Weapon)
		void PullTrigger();

private:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Setup, meta = (AllowPrivateAccess = "true"))
		AGunActor* Gun = nullptr;

};

// Copyrigth Dominik Pavlíček.

#include "public/AITask_Patrol.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "public/AComponent_PatrolRoute.h"

EBTNodeResult::Type UAITask_Patrol::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{		
	/***************************
		BP "NextWaypoint_BP"
		Part: GetPatrolPoints
	***************************/
	ControlledPawn = OwnerComp.GetAIOwner()->GetPawn();
		if (!ensure(ControlledPawn))
		{
			UE_LOG(LogTemp, Error, TEXT("AITask_Patrol || ExecuteTask() || NO CONTROLLED PAWN"))
			return EBTNodeResult::Failed;
		}
	UAComponent_PatrolRoute* PatrolRoute = ControlledPawn->FindComponentByClass<UAComponent_PatrolRoute>();
		if (!ensure(PatrolRoute))
		{
			UE_LOG(LogTemp, Error, TEXT("AITask_Patrol || ExecuteTask() || NO PATROL ROUTE COMPONENT"))
			return EBTNodeResult::Failed;
		}
	TArray<AActor*> PatrolPoints = PatrolRoute->GetPatrolPoints();
		if (PatrolPoints.Num() == 0)
		{
			UE_LOG(LogTemp, Warning, TEXT("AITask_Patrol || ExecuteTask() || NO PATROL POINTS"))
			return EBTNodeResult::Failed;
		}

	/***************************
		BP "NextWaypoint_BP"
		Part: SetNextPoint
	***************************/
	BlackboardComponent = OwnerComp.GetBlackboardComponent(); //<----------|| Gets Blackboard Component of OwnerComp (UBehaviourTreeComponent)
		if (!ensure(BlackboardComponent))
		{
			UE_LOG(LogTemp, Error, TEXT("AITask_Patrol || ExecuteTask() || NO BLACKBOARD COMPONENT"))
			return EBTNodeResult::Failed;
		}
	int32 WayPointIndex = BlackboardComponent->GetValueAsInt(Index.SelectedKeyName); //<------------------|| Gets Indexed element (i.e. first, 5th etc.) of BlackboardComponent as INT
	BlackboardComponent->SetValueAsObject(WayPointKey.SelectedKeyName, PatrolPoints[WayPointIndex]); //<--|| Sets PatrolPoint[index] as WayPointKey Object

	/***************************
		BP "NextWaypoint_BP"
		Part: CycleRoute
	***************************/
	int32 NextIndex = (WayPointIndex + 1) % PatrolPoints.Num();
	BlackboardComponent->SetValueAsInt(Index.SelectedKeyName, NextIndex);
	
	return EBTNodeResult::Succeeded;
}
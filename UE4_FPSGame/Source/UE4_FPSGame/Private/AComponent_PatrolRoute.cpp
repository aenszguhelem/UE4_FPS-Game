// Copyrigth Dominik Pavlíček.

#include "public/AComponent_PatrolRoute.h"


// Sets default values for this component's properties
UAComponent_PatrolRoute::UAComponent_PatrolRoute()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UAComponent_PatrolRoute::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UAComponent_PatrolRoute::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

TArray<AActor*>UAComponent_PatrolRoute::GetPatrolPoints() const
{
	return PatrolPoints;
}


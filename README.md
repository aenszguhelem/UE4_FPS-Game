# UE4_FPS-Game
Basic FPS Game created in Unreal Engine 4, powered by C++

## Main points:
* C++ code
* Blueprint managment
* Avanced AI
* HUD
* Procedural generation of levels
* Controlling skeletal animations

## Commiting
* Commiting will be done periodically in order to show all important key moments of development
* Commits are always with propriet name which describes the key moment

## Before you start a game
* Project requires a Starter Content - download into a project

## Disclaimer:
You are welcome to use, play, download and modify this project as you will to, if you keep the original author as Dominik Pavlíček.
